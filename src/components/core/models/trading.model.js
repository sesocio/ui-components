import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line
import { Project } from "./project.model";

export const Trading = ApplicationRecord.extend({
  static: {
    jsonapiType: "project_tradings"
  },
  attrs: {
    loanNextPaymentDate: attr(),
    paymentStatus: attr(),
    lastPriceP2p: attr(),
    projectId: attr(),
    deletedAt: attr(),
    currency: attr(),
    partLastValueOld: attr(),
    partLastValueUnderlying: attr(),
    partLastValueUnderlyingCurrency: attr(),
    partLastValueCurrencyValue: attr(),
    partLastValueSale: attr(),
    partLastValueSaleCurrency: attr(),
    partLastValueSaleCurrencyValue: attr(),
    partLastValueBuy: attr(),
    partLastValueBuyCurrency: attr(),
    partLastValueBuyCurrencyValue: attr(),
    showable: attr(),
    partValueMaximum: attr(),
    partValueMinimum: attr(),
    salePartsTotal: attr(),
    salePartValue: attr(),
    saleAllPartsTotal: attr(),
    saleCurrency: attr(),
    buyPartsTotal: attr(),
    buyPartValue: attr(),
    buyAllPartsTotal: attr(),
    allPartsTotal: attr(),
    buyCurrency: attr(),
    createdAt: attr(),
    updatedAt: attr(),
    variations: attr(),
    originalPartValues: attr(),
    xirr: attr(),
    partLastValue: attr(),
    liquidityProtocolActive: attr(),
    reserveCurrency: attr(),
    connectorBalance: attr(),
    reserveBalance: attr(),
    connectorCurrency: attr(),
    historicTirr: attr(),
    tradingBuyCommission: attr(),
    tradingSaleCommission: attr(),
    cphr: attr(),
    project: belongsTo()
  }
});

export default Trading;