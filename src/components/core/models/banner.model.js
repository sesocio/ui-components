import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";

export const Banner = ApplicationRecord.extend({
    static: {
      jsonapiType: "banners"
    },
    attrs: {
        startDate: attr(),
        endDate: attr(),
        image: attr(),
        description: attr(),
        link: attr(),
        name: attr(),
        status: attr(),
        center: attr(),
        left: attr(),
        right: attr(),
        top: attr(),
        bottom: attr(),
        test: attr(),
        excludedUrl: attr(),
        includedUrls: attr(),
        imageFileName: attr(),
        imageContentType: attr(),
        imageFileSize: attr(),
        imageUpdatedAt: attr(),
    }
});

export default Banner;