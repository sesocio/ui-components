import { attr, hasMany, belongsTo, hasOne } from "spraypaint";
import { ApplicationRecord } from "./application_record";

import { ArticleComment } from "./article_comment.model";
import { Project } from "./project.model";
import { ArticleProject } from "./article_project.model";

export const Article = ApplicationRecord.extend({
  static: {
    jsonapiType: "articles"
  },
  attrs: {
    id: attr(),
    video: attr(),
    published: attr(),
    photoFileName: attr(),
    photoUpdatedAt: attr(),
    createdAt: attr(),
    displayType: attr(),
    newsType: attr(),
    logoFileName: attr(),
    projectId: attr(),
    fromDate: attr(),
    pinned: attr(),
    publishedAt: attr(),
    sendToAll: attr(),
    pressIconFileName: attr(),
    isPressIcon: attr(),
    title: attr(),
    summary: attr(),
    detail: attr(),
    titleId: attr(),
    imagesUrl: attr(),
    commentsCount: attr(),
    projectsId: attr(),
    logoUrl: attr(),

    userLike: attr(),

    // actions: hasMany(),
    articleComments: hasMany(),
    // project: hasMany()
    // userNew: hasMany()
  }
});

export default Article;