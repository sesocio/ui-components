import { attr, hasMany, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";

import { Article } from "./article.model";
import { Project } from "./project.model";

export const ArticleProject = ApplicationRecord.extend({
  static: {
    jsonapiType: "articles_projects"
  },
  attrs: {
    id: attr(),
    projectId: attr(),
    articleId: attr(),
    articles: belongsTo(),
    projects: belongsTo()
  }
});

export default ArticleProject;