import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line
import { User } from "./user.model";

export const UserBankAccounts = ApplicationRecord.extend({
  static: {
    jsonapiType: "user_bank_accounts"
  },
  attrs: {
    id: attr(),
    user_id: attr(),
    uniformBankKey: attr(),
    bankData: attr(),
    bankId: attr(),
    bankAccountType: attr(),
    accountNumber: attr(),
    bankAccountCurrency: attr(),
    name: attr(),
    state: attr(),
    currency: attr(),
    cuit: attr(),
    bankName: attr(),
    swiftCode: attr(),
    routingNumber: attr(),
    address: attr(),
    verified: attr(),
    ownerName: attr(),
    user: belongsTo(),
    owner: attr(),
    alias: attr(),
    titular: attr(),
    ownerBank: attr(),
    ownerCuit: attr(),
    available: attr(),
    destroyable: attr(),
    visible: attr(),
  }
});

export default UserBankAccounts;