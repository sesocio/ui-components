import { SpraypaintBase } from "spraypaint";
const cookie_name_token = process.env.VUE_APP_COOKIE_NAME_TOKEN;

export var ApplicationRecord = SpraypaintBase.extend({
  static: {
    baseUrl: process.env.VUE_APP_API_BASE_URL,
    apiNamespace: process.env.VUE_APP_API_VERSION_URL,
    generateAuthHeader(token) {
      return token;
    }
  }
});

ApplicationRecord.jwtStorage = cookie_name_token;

export default ApplicationRecord;