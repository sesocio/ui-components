import { attr, hasMany } from "spraypaint";
import { ApplicationRecord } from "./application_record";

export const Category = ApplicationRecord.extend({
  static: {
    jsonapiType: "categories"
  },
  attrs: {
    id: attr(),
    name: attr(),
    url: attr(),
    createdAt: attr(),
    updatedAt: attr(),
    deletedAt: attr(),
    title: attr(),
    titleId: attr(),
    order: attr(),
    categoryId: attr(),

    project: hasMany(),
  }
});

export default Category;