import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line
import { Project } from "./project.model";
// eslint-disable-next-line
import { Article } from "./article.model";
// eslint-disable-next-line
import { User } from "./user.model";

export const UserNews = ApplicationRecord.extend({
  static: {
    jsonapiType: "user_news"
  },
  attrs: {
    id: attr(),
    articleId: attr(),
    userId: attr(),
    projectId: attr(),
    saw: attr(),
    createdAt: attr(),
    newsType: attr(),
    actionId: attr(),
    articleCommentId: attr(),
    callerId: attr(),
    freeText: attr(),
    total: attr(),
    freeUrl: attr(),
    newsPosition: attr(),
    article: belongsTo(),
    user: belongsTo(),
    project: belongsTo()
  }
});

export default UserNews;