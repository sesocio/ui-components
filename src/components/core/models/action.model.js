import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line
import { Project } from "./project.model";
import { Actionable } from "./actionable.model";

export const Action = ApplicationRecord.extend({
  static: {
    jsonapiType: "actions",
  },
  attrs: {
    amount: attr(),
    userId: attr(),
    createdAt: attr({ persist: false }),
    deletedAt: attr({ persist: false }),
    actionableType: attr(),
    actionableId: attr(),
    currency: attr(),
    projectId: attr(),
    showDate: attr(),
    partsTotal: attr(),
    partValue: attr(),
    amountWithoutCommission: attr(),
    commissionPlusTax: attr(),
    commission: attr(),
    status: attr(),
    deleteable: attr(),
    voucher: attr(),
    collectableType: attr(),
    bonusType: attr(),
    collectableId: attr(), // only for deposit pending ID reference
    isCredit: attr(),
    isDebit: attr(),
    isOperation: attr(),
    actionableIdRelation: attr(),
    confirmedByUser: attr(),
    canceledReason: attr(),
    transactionHash: attr(),
    confirmationNumber: attr(),
    from: attr(),
    extraInfo: attr(),
    storeName: attr(),
    trxType: attr(),
    movementCurrency: attr(),
    actionableTrading: attr(),
    cancelable: attr(),
    cardCategory: attr(),
    cardAmountOriginal: attr(),
    cardCurrencyOriginal: attr(),
    cardPaymentDetail: attr(),
    cardExtraction: attr(),

    project: belongsTo(),

    // actionable: belongsTo('actionable'),
  },
});

export default Action;
