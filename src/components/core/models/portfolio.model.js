import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line
import { Project } from "./project.model";
import { User } from "./user.model";

export const Portfolio = ApplicationRecord.extend({
  static: {
    jsonapiType: "portfolios"
  },
  attrs: {
    id: attr(),
    projectId: attr(),
    userId: attr(),
    currency: attr(),
    createdAt: attr({ persist: false }),
    deletedAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    partsTotal: attr(),
    invePayment: attr(),
    partsTotalDecimal: attr(),
    partsCommitted: attr(),
    partsCommittedAndPendingWithdrawal: attr(),
    partsVested: attr(),
    theoricalProfitDecimal: attr(),
    totalInvest: attr(),
    historicInvest: attr(),
    valorizedTokenPercentage: attr(),
    status: attr(),
    reinvest: attr(),
    reinvestAmount: attr(),
    partsPendingWithdrawal: attr(),
    partLastValue: attr(),
    partsValue: attr(),
    partValue: attr(),
    partLastDate: attr(),
    blockedPartsTotal: attr(),
    fundingReinvest: attr(),
    fundingReinvestActive: attr(),
    fundingReinvestPartsTotal: attr(),
    partsBlocked: attr(),
    result: attr(),
    yearlyResult: attr(),
    showable: attr(),
    investmentsAmount: attr(),
    profitsAmount: attr(),
    tradingBuyOffers: attr(),
    hasNews: attr(),
    partsAvailable: attr(),
    partsRemainingView: attr(),
    partsRemainingViewJson: attr(),
    lastValue: attr(),
    projectPercentage: attr(),
    iusdLastValue: attr(),
    project: belongsTo(),
    projectTrading: belongsTo(),
    user: belongsTo()
  }
});

export default Portfolio;