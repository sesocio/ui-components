import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line
// import { Actionable } from "./actionable.model";
// import { TradingSaleOriginal } from "./trading_sale_original.model";

export const TradingSale = ApplicationRecord.extend({
  static: {
    jsonapiType: "trading_sales"
  },
  attrs: {
    partValue: attr(),
    project: belongsTo()
  }
});

export default TradingSale;