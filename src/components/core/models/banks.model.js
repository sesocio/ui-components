import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line
import { Project } from "./project.model";

export const Banks = ApplicationRecord.extend({
  static: {
    jsonapiType: "banks"
  },
  attrs: {
    id: attr(),
    code: attr(),
    name: attr(),
    country: attr()
  }
});

export default Banks;