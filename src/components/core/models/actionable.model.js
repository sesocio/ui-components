import { attr, belongsTo, hasOne } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line

// import { TradingBuyOriginal } from "./trading_buy_original.model";
// import { TradingSaleOriginal } from "./trading_sale_original.model";
// import { TradingSale } from "./trading_sale.model";

import Action from "./action.model";

export const Actionable = ApplicationRecord.extend({
  static: {
    jsonapiType: "actionable"
  },
  attrs: {
    action: hasOne({type: Action, name: 'actionable'}),
    // trading_sale: belongsTo({type: TradingSale, name: 'trading_sale'}),
  }
});

export default Actionable; 