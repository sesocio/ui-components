import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line
// import { Actionable } from "./actionable.model";
// import { TradingBuyOriginal } from "./trading_buy_original.model";

export const TradingBuy = ApplicationRecord.extend({
  static: {
    jsonapiType: "trading_buys"
  },
  attrs: {
    partValue: attr(),
    project: belongsTo()
  }
});

export default TradingBuy;