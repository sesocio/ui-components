import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line

export const TradingBuyOriginal = ApplicationRecord.extend({
  static: {
    jsonapiType: "trading_buy_original"
  },
  attrs: {
    deletedAt: attr(),
    projectId: attr(),
    userId: attr(),
    partsTotal: attr(),
    partValue: attr(),
    currency: attr(),
    currencyValue: attr(),
    confirmedPartsTotal: attr(),
    endDate: attr(),
    variation: attr(),
    active: attr(),
    createdAt: attr(),
    updatedAt: attr(),
    ip: attr(),
    analyticsClientId: attr(),
    investmentId: attr(),
    source: attr(),
    medium: attr(),
    campaign: attr(),
    term: attr(),
    tradingBuyType: attr(),
    cryptoId: attr(),
    amount: attr(),
    confirmedAmountWithoutCommission: attr(),
    content: attr(),
    lastTradingId: attr(),
    confirmedCommissionTax: attr(),

    // trading_buy: hasMany(),
    
    // trading: hasMany(),
    // tradingBuys: hasMany(),
    // actions: hasMany(),

    // user: belongsTo(),
    // investment: belongsTo(),
    // crypto: belongsTo(),
  }
});

export default TradingBuyOriginal;