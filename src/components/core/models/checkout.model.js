import { attr, belongsTo, hasOne } from "spraypaint";
import { ApplicationRecord } from "./application_record";

export const Checkout = ApplicationRecord.extend({
  static: {
    jsonapiType: "checkouts",
  },
  attrs: {
    projectId: attr(),
    sessionId: attr(),
    currency: attr(),
    country: attr(),
    amount: attr(),
    partsTotal: attr(),
    curveExtraPercentage: attr(),
    status: attr(),
    attempts: attr(),
    curveExtraPercentageOriginal: attr(),
    source: attr(),
    medium: attr(),
    campaign: attr(),
    term: attr(),
    fromCollection: attr(),
    mpUrl: attr(),
    tpUrl: attr(),
    partsTotalView: attr(),
    collectableId: attr(),
    userPaymentMethodId: attr(),
    paymentMethodId: attr(),
    token: attr(),
    partsTotalCurveExtraView: attr(),

    project: belongsTo(),
    user: belongsTo(),

    investment: hasOne(),
  },
});

export default Checkout;