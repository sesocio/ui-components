import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";

export const VestingAssignments = ApplicationRecord.extend({
  static: {
    jsonapiType: "vesting_assignments"
  },
  attrs: {
    partsTotal: attr(),
    date: attr(),
    status: attr()
  }
});

export default VestingAssignments;