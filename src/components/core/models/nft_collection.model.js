import { attr, belongsTo, hasMany } from "spraypaint";
import { ApplicationRecord } from "./application_record";

export const NftCollection = ApplicationRecord.extend({
  static: {
    jsonapiType: "nft_collections"
  },
  attrs: {
    id: attr(),
    name: attr(),
    nameId: attr(),
    logoUrl: attr(),
    backgroundsUrl: attr(),
    bannersUrl: attr(),
    summary: attr(),

    project: hasMany(),
    nftAuthor: belongsTo(),
  }
});

export default NftCollection;