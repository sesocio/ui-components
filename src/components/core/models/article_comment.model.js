import { attr, hasMany, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";

import { Article } from "./article.model";
import { User } from "./user.model";

export const ArticleComment = ApplicationRecord.extend({
  static: {
    jsonapiType: "article_comments"
  },
  attrs: {
    id: attr(),
    articleId: attr(),
    parentId: attr(),
    userComment: attr(),
    published: attr(),
    createdAt: attr(),
    commentType: attr(),
    reportedCount: attr(),
    cachedVotesUp: attr(),
    cachedVotesDown: attr(),
    childCommentsCount: attr(),
    currentUserLike: attr(),
    currentUserDislike: attr(),
    as_sesocio: attr(),

    replies: attr(), // para cargar respuestas

    article: belongsTo(),
    user: belongsTo()
  }
});

export default ArticleComment;