import { attr, belongsTo, hasMany } from "spraypaint";
import { ApplicationRecord } from "./application_record";
// eslint-disable-next-line

export const TradingSaleOriginal = ApplicationRecord.extend({
  static: {
    jsonapiType: "trading_sale_original"
  },
  attrs: {
    deletedAt: attr(),
    projectId: attr(),
    userId: attr(),
    portfolioId: attr(),
    partsTotal: attr(),
    partValue: attr(),
    currency: attr(),
    currencyValue: attr(),
    confirmedPartsTotal: attr(),
    endDate: attr(),
    variation: attr(),
    active: attr(),
    createdAt: attr(),
    updatedAt: attr(),
    ip: attr(),
    analyticsClientId: attr(),
    tradingSaleType: attr(),
    cryptoId: attr(),
    confirmedAmountWithoutCommission: attr(),
    lastTradingId: attr(),
    confirmedCommissionTax: attr(),

    // trading_sale: hasMany(),
  }
});

export default TradingSaleOriginal;