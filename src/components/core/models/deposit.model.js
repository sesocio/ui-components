import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";

export const Deposit = ApplicationRecord.extend({
  static: {
    jsonapiType: "deposits"
  },
  attrs: {
    amount: attr(),
    amountWwithoutCommission: attr(),
    commission: attr(),
    commissionTax: attr(),
    currency: attr(),
    currencyValue: attr(),
    currencyTo: attr(),
    currencyToValue: attr(),
    status: attr(),
    comment: attr(),
    ip: attr(),
    analyticsClientId: attr(),
    confirmedDate: attr(),
    createdAt: attr({ persist: false }),
    updatedAt: attr({ persist: false }),
    deletedAt: attr({ persist: false }),
    newAmount: attr(),
    collectionAmount: attr(),
    userCashAmountBefore: attr(),
    userCashAmountAfter: attr(),
    userPaymentMethodId: attr(),
    userCurrency: attr(),
    mktCost: attr(),
    partsTotal: attr(),
    portfolioAmountBefore: attr(),
    portfolioAmountAfter: attr(),
    portfolioCurrency: attr(),
    type: attr(),
    bankAccountId: attr(),
    from_pwa: attr(),
    projectId: attr(),
    investmentId: attr(),
    userId: attr(),
    method: attr(),
    collectableId: attr(),
    user: belongsTo()
  }
});

export default Deposit;