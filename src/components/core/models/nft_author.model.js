import { attr, hasMany } from "spraypaint";
import { ApplicationRecord } from "./application_record";

export const NftAuthor = ApplicationRecord.extend({
  static: {
    jsonapiType: "nft_authors"
  },
  attrs: {
    id: attr(),
    name: attr(),
    photosUrl: attr(),

    nftCollection: hasMany(),
  }
});

export default NftAuthor;