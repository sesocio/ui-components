import { attr, belongsTo } from "spraypaint";
import { ApplicationRecord } from "./application_record";

export const Ranking = ApplicationRecord.extend({
  static: {
    jsonapiType: "social_network_rankings"
  },
  attrs: {
    userId: attr(),
    userAlias: attr(),
    userPhoto: attr(),
    userCountry: attr(),
    investmentsAmount: attr(),
    investmentsPercentage: attr(),
    commentsScore: attr(),
    totalScore: attr(),
    rankingScore: attr(),
    ranking: attr(),
    totalComments: attr(),
    positiveComments: attr(),
    negativeComments: attr(),
    positiveCommentsWeighted: attr(),
    negativeCommentsWeighted: attr(),
    variationRanking: attr(),
    image: attr(),
    user: belongsTo()
  }
});

export default Ranking;