const SET_REMAINDER = (state, payload) => {
    state.reminder = payload;
};
const SET_LOADER = (state, payload) => {
    state.loading = payload;
};

const SET_SELECTED_CURRENCY_WALLET = (state, data) => {
    state.selectedCurrencyWallet = data;
};

const SET_SELECTED_CURRENCY_BANK_ACCOUNT = (state, data) => {
    state.selectedCurrencyBankAccount = data;
};

const SET_SELECTED_CURRENCY_BANK_ACCOUNT_2 = (state, data) => {
    state.selectedCurrencyBankAccount2 = data;
};

export default {
    SET_REMAINDER,
    SET_SELECTED_CURRENCY_WALLET,
    SET_SELECTED_CURRENCY_BANK_ACCOUNT,
    SET_SELECTED_CURRENCY_BANK_ACCOUNT_2,
    SET_LOADER
};