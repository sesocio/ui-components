import actions from "./actions";
import mutations from "./mutations";

const state = {
    from: "",
    reminder: "",
    loading: false,
    selectedCurrencyWallet: "",
    selectedCurrencyBankAccount: "",
    selectedCurrencyBankAccount2: "",
    valued_tokens_by_project: "",
    valued_tokens_history: "",
    dashboard_project: "",
    dashboard_cryptos: "",
    dashboard_trading: "",
    valued_tokens_24_variations: 0
};

export default {
    namespaced: true,
    state,
    actions,
    mutations
};
