import {Action} from "../../core/models/action.model";

const getPendingActions = async ({commit}) => {
    let action = await Action.selectExtra(["deleteable", "cancelable"]).where({
        'actionable_type': 'Deposit',
        'status': 'pending'
    }).order({show_date: "asc"}).first();
    let data = "";
    if (action.data != null) {
        data = action.data;
    }
    commit('SET_REMAINDER', data);
};

const clearReminder = async ({commit}) => {
    let data = "";
    await commit('SET_REMAINDER', data);
};

const getSelectedCurrencyWallet = async ({commit, dispatch}, currency) => {
    try {
        commit("SET_LOADER", true);
        let json = {params: {currency: currency}};
        await Axios.get(
            process.env.VUE_APP_API_BASE_URL +
            process.env.VUE_APP_API_VERSION_URL +
            "/users/get_crypto_wallet",
            json
        )
            .then(function (resp) {
                commit("SET_LOADER", false);
                const data = resp.data;
                commit("SET_SELECTED_CURRENCY_WALLET", data);
            })
            .catch(function () {
                commit("SET_LOADER", false);
            });
    } catch (error) {
        return error.response;
    }
};

const getSelectedCurrencyBankAccount = async (
    {commit, dispatch},
    obj
) => {
    try {
        commit("SET_LOADER", true);
        let amount = (obj.amount === undefined) ? '' : obj.amount;
        let currency = (obj.amount === undefined) ? obj : obj.currency;
        let project = (obj.projectId === undefined) ? '' : obj.projectId
        let json = {params: {currency: currency, amount: amount, account_type: "cbu", project_id: project}};
        await Axios.get(
            process.env.VUE_APP_API_BASE_URL +
            process.env.VUE_APP_API_VERSION_URL +
            "/bank_accounts/bank_account_for_user",
            json
        )
            .then(function (resp) {
                commit("SET_LOADER", false);
                const data = resp.data;
                commit("SET_SELECTED_CURRENCY_BANK_ACCOUNT", data);
            })
            .catch(function () {
                commit("SET_LOADER", false);
            });
    } catch (error) {
        return error.response;
    }
};

const getSelectedCurrencyBankAccount2 = async (
    {commit, dispatch},
    obj
) => {
    try {
        commit("SET_SELECTED_CURRENCY_BANK_ACCOUNT_2", "");
        commit("SET_LOADER", true);
        let amount = (obj.amount === undefined) ? '' : obj.amount;
        let currency = (obj.amount === undefined) ? obj : obj.currency;
        let json = {params: {currency: currency, amount: amount, account_type: "cvu"}};
        await Axios.get(
            process.env.VUE_APP_API_BASE_URL +
            process.env.VUE_APP_API_VERSION_URL +
            "/bank_accounts/bank_account_for_user",
            json
        )
            .then(function (resp) {
                commit("SET_LOADER", false);
                const data = resp.data;
                commit("SET_SELECTED_CURRENCY_BANK_ACCOUNT_2", data);
            })
            .catch(function () {
                commit("SET_LOADER", false);
            });
    } catch (error) {
        return error.response;
    }
};

export default {
    getPendingActions,
    clearReminder,
    getSelectedCurrencyWallet,
    getSelectedCurrencyBankAccount,
    getSelectedCurrencyBankAccount2,
};
