const LOGIN = state => {
  state.status = "loading";
};
const LOGIN_SUCCESS = (state, token) => {
  state.isLoggedIn = true;
  state.status = "success";
};
const LOGIN_ERROR = state => {
  state.isLoggedIn = false;
  state.status = "error";
};
const LOGOUT = state => {
  state.isLoggedIn = false;
  state.status = "";
  state.userRequiredData = null;
  state.shippingAddress = null;
  state.current = {};
};

const SET_CURRENT = (state, payload) => {
  state.current = payload.current.attributes;
};

const SET_USER_LATAM_MILES_DETAIL = (state, payload) => {
  state.user_latam_miles_detail = payload;
};

const SET_USER_ID = (state, payload) => {
  state.userId = payload;
};

const SET_USER_REQUIRED_DATA = (state, payload) => {
  state.userRequiredData = payload;
};

const SET_SHIPPING_ADDRESS = (state, payload) => {
  state.shippingAddress = payload;
};

const SET_SHIPPING_ADDRESS_STATUS = (state, payload) => {
  state.shippingAddressStatus = payload.result;
};

const SET_NEWS = (state, data) => {
  state.news = data;
};

const SET_NEWS_LIST = (state, data) => {
  state.news_list = data;
  state.lastPage = state.news_list.length === 0;
};

const SET_NEWS_PAGE = (state, { newsPage }) => {
  state.newsPage = newsPage;
};

const SET_BANK_ACCOUNTS = (state, payload) => {
  state.userBankAccounts = payload;
};

const CLEAN_BANK_ACCOUNTS = state => {
  state.userBankAccounts = "";
};

const SET_BANKS_LIST = (state, payload) => {
  state.banksList = payload;
};

const SET_ACCOUNT_API_RESPONSE = (state, payload) => {
  state.accountApiResponse = payload;
};

const SET_USER_API_RESPONSE = (state, payload) => {
  state.userApiResponse = payload;
};

const SET_TOTAL_ITEMS = (state, payload) => {
  state.totalItems = payload;
};

const SET_LAST_PAGE = (state, payload) => {
  state.lastPage = payload;
};

const SET_CURRENT_PAGE = (state, payload) => {
  state.currentPage = payload;
};

const SET_DOCS_FIELDS = (state, payload) => {
  state.docs_fields = payload;
};

const SET_LOADING_BANK_DATA = (state, payload) => {
  state.loadingBankData = payload;
};

const SET_LOADING_DOCS = (state, payload) => {
  state.loadingDocs = payload;
};

const SET_SHOW_HIDE_GET_PHONE_DIALOG = (state, payload) => {
  state.get_phone_dialog = payload;
}

const SET_UPDATING_PHONE = (state, payload) => {
  state.updating_phone = payload;
}

const SET_PHONE_UPDATED = (state, payload) => {
  state.phone_updated = payload;
}

const SET_PHONE = (state, payload) => {
  state.current.phone = payload;
}

const SET_LOADER = (state, payload) => {
  state.loading_news = payload;
};

const SET_LIST_LOADER = (state, payload) => {
  state.loading_news_list = payload;
};

const CLEAN_NEWS = (state) => {
  state.news_list = [];
  state.currentPage = 1;
  state.lastPage = false;
};

const SET_USER_STATISTICS = (state, payload) => {
  let arrQ = Object.keys(payload);
  if(arrQ.length > 0){
    let arr = ['utm_source', 'utm_campaign', 'utm_medium', 'utm_term', 'utm_content', 'utm_from'];
    for (var i = 0; i < arrQ.length; i++) {
      if(arr.indexOf(arrQ[i]) >= 0){
        state.user_statistic_attributes[arrQ[i].replace('utm_', '')] = payload[arrQ[i]];
      }
    }
  }
};

const SET_IS_HATER = (state, payload) => {
  state.is_hater = payload;
};

const SET_VERIFYING_PHONE = (state, payload) => {
  state.verifying_phone = payload;
}

const SET_VERIFIED_PHONE = (state, payload) => {
  state.current.verified_phone = payload;
}

const SET_NEW_PHONE = (state, payload) => {
  state.current.new_phone = payload;
}

const SET_CAN_EDIT_PHONE = (state, payload) => {
  state.can_edit_phone = payload;
}

const SET_INVALID_CODE = (state, payload) => {
  state.invalid_code = payload;
}

const SET_VERIFIED_BANK_ACCOUNT = (state, payload) => {
  state.verified_bank_account = payload;
}

const SET_CURRENT_BANK_ACCOUNT = (state, payload) => {
  state.currentBankAccount = payload
}

const SET_TRANSACTIONS = (state, data) => {
  state.transactions = data;
};

const SET_SELECTED_ITEM = (state, payload) => {
  state.selectedItem = payload;
};

const CLEAR_TRANSACTIONS = state => {
  state.transactions = [];
  state.selectedItem = "";
  state.currentPage = 0;
  state.lastPage = 0;
  state.totalItems = 0;
  state.dialog = false;
};


export default {
  LOGIN,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGOUT,
  SET_CURRENT,
  SET_USER_LATAM_MILES_DETAIL,
  SET_USER_ID,
  SET_USER_REQUIRED_DATA,
  SET_SHIPPING_ADDRESS,
  SET_SHIPPING_ADDRESS_STATUS,
  SET_NEWS,
  SET_NEWS_LIST,
  SET_NEWS_PAGE,
  SET_BANK_ACCOUNTS,
  CLEAN_BANK_ACCOUNTS,
  SET_BANKS_LIST,
  SET_ACCOUNT_API_RESPONSE,
  SET_USER_API_RESPONSE,
  SET_TOTAL_ITEMS,
  SET_LAST_PAGE,
  SET_CURRENT_PAGE,
  SET_DOCS_FIELDS,
  SET_LOADING_BANK_DATA,
  SET_LOADING_DOCS,
  SET_SHOW_HIDE_GET_PHONE_DIALOG,
  SET_UPDATING_PHONE,
  SET_PHONE_UPDATED,
  SET_PHONE,
  SET_LOADER,
  SET_LIST_LOADER,
  CLEAN_NEWS,
  SET_USER_STATISTICS,
  SET_IS_HATER,
  SET_VERIFYING_PHONE,
  SET_VERIFIED_PHONE,
  SET_NEW_PHONE,
  SET_CAN_EDIT_PHONE,
  SET_INVALID_CODE,
  SET_VERIFIED_BANK_ACCOUNT,
  SET_CURRENT_BANK_ACCOUNT,
  SET_TRANSACTIONS,
  SET_SELECTED_ITEM,
  CLEAR_TRANSACTIONS
};
