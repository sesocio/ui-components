import cookie from 'vue-cookies'
import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";
const cookie_name_token = process.env.VUE_APP_COOKIE_NAME_TOKEN;

const state = {
  transactions: [],
  selectedItem: [],
  status: "",
  isLoggedIn: !!cookie.get(cookie_name_token) || !!localStorage.getItem(cookie_name_token),
  current: {},
  is_hater: false,
  userId: "",
  news: [],
  news_list: [],
  newsPage: 0,
  currentPage: 0,
  totalItems: 0,
  lastPage: 0,
  loading: false,
  userRequiredData: null,
  shippingAddress: null,
  shippingAddressStatus: null,
  userBankAccounts: "",
  filterBankAccounts: "",
  filterBankAccountsReceipt: "",
  accountApiResponse: "",
  userApiResponse: "",
  currentBankAccount: {},
  bankFieldsByCountry: {
    ar: {
      name: "",
      titular: "",
      ownerCuit: "",
      bankAccountType: [
        {
          id: "caja_ahorro", value: "Caja de Ahorros"
        },
        {
          id: "cuenta_corriente", value: "Cuenta Corriente"
        },
        {
          id: "cuenta_virtual", value: "Cuenta Virtual"
        }
      ],
      alias: "",
      uniformBankKey: "",
      ownerBank: "",
      currency: [
        {
          id: "ARS", value: "ARS"
        },
        {
          id: "USD", value: "USD"
        }
      ]
    },
    cl: {
      bankId: "",
      accountNumber: "",
      currency: [
        {
          id: "CLP", value: "CLP"
        }
      ],
      name: ""
    },
    co: {
      bankName: "",
      accountNumber: "",
      bankAccountType: [
        {
          id: "caja_ahorro", value: "Caja de Ahorros"
        },
        {
          id: "cuenta_corriente", value: "Cuenta Corriente"
        },
        {
          id: "cuenta_virtual", value: "Cuenta Virtual"
        }
      ],
      currency: [
        {
          id: "COP", value: "COP"
        },
      ],
      swiftCode: "",
      address: "",
      name: ""
    },
    mx: {
      bankName: "",
      accountNumber: "",
      currency: [
        {
          id: "MXN", value: "MXN"
        },
      ],
      uniformBankKey: "",
      name: ""
    },
    pe: {
      bankName: "",
      accountNumber: "",
      bankAccountType: [
        {
          id: "caja_ahorro", value: "Caja de Ahorros"
        },
        {
          id: "cuenta_corriente", value: "Cuenta Corriente"
        },
        {
          id: "cuenta_virtual", value: "Cuenta Virtual"
        }
      ],
      currency: [
        {
          id: "PEN", value: "PEN"
        },
        {
          id: "USD", value: "USD"
        }
      ],
      swiftCode: "",
      address: "",
      uniformBankKey: "",
      name: ""
    },
    default: {
      bankName: "",
      accountNumber: "",
      bankAccountType: [
        {
          id: "caja_ahorro", value: "Caja de Ahorros"
        },
        {
          id: "cuenta_corriente", value: "Cuenta Corriente"
        },
        {
          id: "cuenta_virtual", value: "Cuenta Virtual"
        }
      ],
      currency: [
        {
          id: "USD", value: "USD"
        }
      ], // current user + USD (?)
      swiftCode: "",
      routingNumber: "",
      address: "",
      name: ""
    },
  },
  banksList: [],
  docs_fields: {},
  loadingBankData: true,
  loadingDocs: true,
  get_phone_dialog: false,
  updating_phone: false,
  phone_updated: false,
  loading_news: false,
  loading_news_list: false,
  user_statistic_attributes: {
    source: "",
    campaign: "",
    medium: "",
    term: "",
    content: ""
  },
  user_latam_miles_detail: [],
  verifying_phone: false,
  can_edit_phone: false,
  invalid_code: false,
  verified_bank_account: false,
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};
