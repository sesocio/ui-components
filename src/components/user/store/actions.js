import cookie from 'vue-cookies'
import Axios from "axios";
import { ApplicationRecord } from "../../core/models/application_record";
import { User } from "../../core/models/user.model";
import { UserNews } from "../../core/models/user.news.model";
import { UserBankAccounts } from "../../core/models/user_bank_account.model";
import { Banks } from "../../core/models/banks.model";
import { Action } from "../../core/models/action.model";
import { Project } from "../../core/models/project.model";
import { Withdrawal } from "../../core/models/withdrawal.model";
import i18n from '@/i18n.js';
import FingerprintJS from '@fingerprintjs/fingerprintjs';

const cookie_name_token = process.env.VUE_APP_COOKIE_NAME_TOKEN;

const login = async ({ commit, dispatch }, user) => {
  const fpPromise = FingerprintJS.load()
  const fp = await fpPromise
  const result = await fp.get()
  const visitorId = result.visitorId
  user.visitorId = visitorId

  return new Promise(async(resolve, reject)=>{
    try {
      window.localStorage.removeItem(cookie_name_token);
      window.localStorage.removeItem("vuex");
      let url = `${process.env.VUE_APP_API_BASE_URL}/auth/login`;

      const response = await Axios.post(url, user)
      const token = response.data.token;
      await dispatch("setToken", token);

      resolve(response)

    } catch (e) {
      if(e.response.data && e.response.data.error == 'pwa_not_active'){
        location.href = `${process.env.VUE_APP_API_BASE_URL}/my_home?token=${e.response.data.token}`;
        reject(e)
      }
      commit("LOGIN_ERROR");
      window.localStorage.removeItem(cookie_name_token);
      window.localStorage.removeItem("vuex");

      reject(e)
    }
  })
};

// const login = async ({ commit, dispatch }, user) => {
//   window.localStorage.removeItem(cookie_name_token);
//   window.localStorage.removeItem("vuex");

//   commit("LOGIN"); // show spinner
//   dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
//   try {
//     let resp = await Axios({
//       url: process.env.VUE_APP_API_BASE_URL + "/auth/login",
//       data: user,
//       method: "POST"
//     });
//     const token = resp.data.token;

//     dispatch("setToken", token);

//     dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
//     return resp;
//   } catch (error) {
//     if(error.response.data && error.response.data.error == 'pwa_not_active'){
//       location.href = process.env.VUE_APP_API_BASE_URL + '/my_home?token=' + error.response.data.token;
//       return;
//     }
//     commit("LOGIN_ERROR");
//     window.localStorage.removeItem(cookie_name_token);
//     window.localStorage.removeItem("vuex");
//     dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
//     return error.response;
//   }
// };

const forgotPassword = async ({ commit, dispatch }, email) => {
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/users/reset_password",
      data: email,
      method: "POST"
    });
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    return resp;
  } catch (error) {
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    return error.response;
  }
};

const resetPassword = async ({ commit, dispatch }, params) => {
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/users/reset_password_by_token",
      data: params,
      method: "POST"
    });
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    return resp;
  } catch (error) {
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    return error.response;
  }
};

const register = async ({ commit, dispatch }, params) => {
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/users/",
      data: {user: params},
      method: "POST"
    });
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    if(resp.data.hasOwnProperty('token')){
      dispatch("setToken", resp.data.token);
    }
    return resp;
  } catch (error) {
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    return error.response;
  }
};

const setToken = async ({ commit, dispatch }, token) => {
  localStorage.setItem(cookie_name_token, token);
  if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {

    let cookie_domain = process.env.VUE_APP_COOKIE_DOMAIN;
    let domain = window.location.hostname;
    domain = domain.replace('www.','');
    if(domain == 'sesocia.com') {
      cookie_domain = domain;
    }

    cookie.set(cookie_name_token, token, '1d', '', cookie_domain, true, 'Strict');
  } else {
    cookie.set(cookie_name_token, token, '1d');
  }
  Axios.defaults.headers.common["Authorization"] = token;
  ApplicationRecord.jwt = token;
  await dispatch("getCurrent");
  commit("LOGIN_SUCCESS", token);
  return token;
};

const checkIsLogin = async ({ commit, state, dispatch }) => {
  let auxReturn = false;
  let token = cookie.get(cookie_name_token) || localStorage.getItem(cookie_name_token);
  if (token && state.isLoggedIn) {
    await dispatch("setToken", token);
    auxReturn = true;
  }else {
    dispatch('logout');
  }
  return auxReturn;
};

const setTokenFromQuery = async ({ commit, dispatch }, token) => {
  localStorage.setItem(cookie_name_token, token);
  cookie.set(cookie_name_token, token, '1d', '', process.env.VUE_APP_COOKIE_DOMAIN, true, 'Strict');
  Axios.defaults.headers.common["Authorization"] = token;
  ApplicationRecord.jwt = token;
  commit("LOGIN_SUCCESS", token);
  dispatch("getCurrent");
  return token;
}

const logout = ({ commit }) => {
  commit("LOGOUT");
  window.localStorage.clear();
  window.localStorage.removeItem("vuex");

  let cookie_domain = process.env.VUE_APP_COOKIE_DOMAIN;
  let domain = window.location.hostname;
  domain = domain.replace('www.','');
  if(domain == 'sesocia.com') {
    cookie_domain = domain;
  }

  if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
    cookie.remove(cookie_name_token, null, cookie_domain);
  }else {
    cookie.remove(cookie_name_token);
  }

  delete Axios.defaults.headers.common["Authorization"];
  ApplicationRecord.jwt = null;
  // location.reload();
};

const getCurrent = async ({ dispatch, commit }) => {
  try {
    let { raw } = await User.select(['address','email','city','state','street','street_number','postal_code',
    'country','phone','old_phone','document','currency','firstname','lastname','cuit','provider','provider_photo',
    'photo','alias','local_currency','facta', 'obligated_subject', 'legal_entity', 'politically_exposed','charge','family_politically_exposed',
    'relation_politically_exposed','name_family_politically_exposed','display_currency',
    'otp_secret_key','otp_active','latam','latam_miles','rut','declarant','locale',
    'banned_type','newsletter_support_type','newsletter_user_owner_email',
    'first_investment_date','total_amount','last_new_saw', 'verified_phone', 'phone_verification_step', 'new_phone', 'gross_annual_income', 'sesocio_user', 'tax', 'confirmed_at', 'limit_card', 'birthdate', 'gender', 'email_verification', 'allow_whatsapp', 'otp_activated_at', 'otp_activations', 'occupation', 'civil_status', 'card_company_id', 'created_at'])
    .selectExtra(["all_documents_approved","all_documents_approved_for_withdrawal","must_upload_documents", "rejected_documents",
    "all_documents_checking", "profile_data_completed", "comment_as_sesocio",
    "newsletter_support_type", "newsletter_user_owner_email", "user_referrer_name",
    "newsletter_created_at", "can_comment_social_network", "limit_alert", "crypto_wallet", "has_prepaid_card", "has_deposits"]).first();
    commit("SET_USER_ID", raw.data[0].id);

    if(raw.data[0].attributes.banned_type.includes('banned')){
      dispatch('logout');
    }

    commit("SET_CURRENT", {
      current: raw.data[0]
    });
    i18n.locale = raw.data[0].attributes.locale;
  } catch (e) {
      if(e.response.hasOwnProperty('status') && e.response.status == 401){
        dispatch('logout');
      }
  }
};

const checkIsHater = async ({ dispatch, commit }) => {
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  try {
    let { data } = await User.select(['country']).selectExtra(["is_hater"]).first();
    commit("SET_IS_HATER", data.isHater);
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  } catch (e) {
      console.log(e);
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  }
};

const getCurrentLatamMilesDetail = async ({ commit }) => {
  try {
    await Axios.get(process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/users/get_latam_miles_detail")
    .then((resp) => {
      if(resp.status === 200 && resp.data.hasOwnProperty('miles_detail')) {
        commit("SET_USER_LATAM_MILES_DETAIL", resp.data.miles_detail);
      }
    })
    .catch((e) => {
      console.log(e);
    });
  } catch (e) {
    console.log(e);
  }
};

const getNews = async ({ commit, state, dispatch }, limit) => {
  commit('SET_LOADER', true);
  let resp = await Axios({
    url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + `/user_news/get_user_news/${limit}/0`,
    method: "GET"
  });
  commit("SET_NEWS", resp.data);
  commit('SET_LOADER', false);
};

const updateSaw = async ({ commit, state, dispatch }, news) => {
  commit("SET_NEWS", news);
};

const getNewsList = async ({ commit, state }, type) => {
  let limit = 50;
  let offset = (state.currentPage - 1) * limit;
  type = null;
  commit('SET_LIST_LOADER', true);
  try {
    let url = process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL +
      `/user_news/get_user_news/${limit}/${offset}`;
    await Axios.get(url, {params: {type: type}}).then(function(resp) {
        commit("SET_NEWS_LIST", resp.data);
      }).catch(function(e) {
        console.log(e);
      });
  } catch (err) {
    return err;
  }
  commit('SET_LIST_LOADER', false);
};

const clearNewsList = async ({ commit, state, dispatch }) => {
  commit("CLEAN_NEWS");
};

const getUserBankAccounts = async ({ commit, state, dispatch }, currency) => {
  commit("SET_LOADING_BANK_DATA", true);
  let accounts = {};
  if(currency != ''){
    accounts = await UserBankAccounts
      .where({currency: currency})
      .all();
  } else {
    accounts = await UserBankAccounts.all();
  }
  commit("SET_BANK_ACCOUNTS", accounts.data);
  commit("SET_LOADING_BANK_DATA", false);
};

const getVerifiedUserBankAccounts = async ({ commit, state, dispatch }, currency) => {
  commit("SET_LOADING_BANK_DATA", true);
  let accounts = {};
  if(currency != ''){
    accounts = await UserBankAccounts
      .where({verified: true, currency: currency})
      .selectExtra(["owner_name", "owned_by_user"])
      .all();
  } else {
    accounts = await UserBankAccounts
      .where({verified: true})
      .selectExtra(["owner_name", "owned_by_user"])
      .all();
  }
  commit("SET_BANK_ACCOUNTS", accounts.data);
  commit("SET_LOADING_BANK_DATA", false);
};

const cleanUserBankAccounts = async ({ commit, state, dispatch }) => {
  commit("CLEAN_BANK_ACCOUNTS");
};

const getBanksList = async ({ commit, state }, country) => {
  let banks;
  // console.log(country);
  if(country){
    banks = await Banks.where({country: country}).all();
  } else {
    banks = await Banks.all();
  }
  commit("SET_BANKS_LIST", banks.data);
};

const setUserNewBankAccount = async ({ commit, state, dispatch }, data) => {
  // dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  let account;
  if(data.data.isPersisted){
    account = new UserBankAccounts({id: data.data.id});
    account.isPersisted = true;
  } else {
    account = new UserBankAccounts();
  }

  for(var f in data.fields){
    account[f] = data.data[f];
  }

  try {
    await account
    .save()
    .then(function(success) {
      if (!success) {
        commit("SET_ACCOUNT_API_RESPONSE", { errors: account.errors });
      } else {
        dispatch("getUserBankAccounts");
        commit("SET_ACCOUNT_API_RESPONSE", account);
      }
      // dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    })
    .catch(function(e) {
      commit("SET_ACCOUNT_API_RESPONSE", { errors: e.response });
      // dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    });
  } catch (e) {
    // dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  }
};

const deleteUserBankAccount = async ({ commit, state, dispatch }, data) => {
  // dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  let account;
  account = new UserBankAccounts({id: data.data.id});
  account.isPersisted = true;
  try {
    await account
    .destroy()
    .then(function(success) {
      if (!success) {
        commit("SET_ACCOUNT_API_RESPONSE", { errors: account.errors });
      } else {
        dispatch("getUserBankAccounts");
        commit("SET_ACCOUNT_API_RESPONSE", account);
      }
      // dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    })
    .catch(function(e) {
      commit("SET_ACCOUNT_API_RESPONSE", { errors: e.response });
      // dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    });
  } catch (e) {
    // dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  }

};

const updateVisibleUserBankAccount = async ({ commit, state, dispatch }, data) => {
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/user_bank_accounts/"+ data.data.id +"/update_visible",
      data: data,
      method: "PATCH"
    });
    dispatch("getUserBankAccounts");
    return resp;
  } catch (e) {
    commit("SET_ACCOUNT_API_RESPONSE", { errors: e.response });
  }
};

const setUserData = async ({ commit, state, dispatch }, data) => {
  // console.log(data);
  // return;
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  let user = new User({id: state.userId});
  user.isPersisted = true;

  user.firstname = data.firstname;
  user.lastname = data.lastname;
  user.country = data.country;
  user.state = data.state;
  user.city = data.city;
  user.declarant = data.declarant;
  user.rut = data.rut;
  user.address = data.address;
  user.street = data.street;
  user.streetNumber = data.streetNumber;
  user.streetFloor = data.streetFloor;
  user.streetFloorApartment = data.streetFloorApartment;
  user.department = data.department;
  user.postalCode = data.postalCode;
  user.addressLocation = data.addressLocation;
  user.document = data.document;
  user.typeDocument = data.typeDocument;
  if(data.country.toLowerCase() == 'ar'){
    user.cuit = data.cuit;
  }
  // user.email = data.email;
  // user.phone = data.phone;
  user.birthdate = data.birthdate;
  user.gender = data.gender;
  user.occupation = data.occupation;
  user.civilStatus = data.civilStatus;
  user.avatar = data.avatar;
  // user.currency = data.currency;
  user.alias = data.alias;
  // user.latam = data.latam;
  user.grossAnnualIncome = data.gross_annual_income;
  user.locale = data.locale;
  user.politicallyExposed = data.politically_exposed;
  user.facta = data.facta;
  user.obligatedSubject = data.obligatedSubject || data.obligated_subject;
  user.legalEntity = data.legalEntity || data.legal_entity;

  user.charge = data.charge;

  user.relationPoliticallyExposed = data.relation_politically_exposed;
  user.familyPoliticallyExposed = data.family_politically_exposed;
  user.nameFamilyPoliticallyExposed = data.name_family_politically_exposed;

  user.allowWhatsapp = data.allow_whatsapp;

  try {
    await user
    .save()
    .then(async function(success) {
      if (!success) {
        commit("SET_USER_API_RESPONSE", { errors: user.errors });
      } else {
        // commit("SET_CURRENT", {current: user});
        await dispatch('getCurrent');
        commit("SET_USER_API_RESPONSE", user);
      }
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    })
    .catch(function(e) {
      commit("SET_USER_API_RESPONSE", { errors: e.response });
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    });
  } catch (e) {
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  }
}

const getDocFields = async ({ commit, dispatch }) => {
  commit("SET_LOADING_DOCS", true);
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/documents",
      method: "GET"
    });
    const data = resp.data;
    commit("SET_DOCS_FIELDS", data);
    commit("SET_LOADING_DOCS", false);
    return resp;
  } catch (error) {
    commit("SET_DOCS_FIELDS", "");
    commit("SET_LOADING_DOCS", false);
  }
};

const saveDocFiels = async ({ commit, dispatch }, data) => {
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/documents",
      data: data,
      method: "POST"
    });
    // const data = resp.data;
    console.log(resp);
    // commit("SET_DOCS_FIELDS", data);
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    return resp;
  } catch (error) {
    // commit("SET_DOCS_FIELDS", "");
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  }
};

const saveAvatar = async ({ commit, dispatch }, data) => {
  dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/documents",
      data: data,
      method: "POST"
    });
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
    dispatch("getCurrent");
    return resp;
  } catch (error) {
    dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  }
};

const setShowHideGetPhoneDialog = ({ commit }, status) => {
  commit("SET_SHOW_HIDE_GET_PHONE_DIALOG", status);
};

const setUserStatistics = ({ commit }, json) => {
  commit("SET_USER_STATISTICS", json);
};

const updatePhone = async ({ commit, state }, data) => {
  commit("SET_UPDATING_PHONE", true);
  try {
    await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/users/phone",
      data: {phone:data.phone},
      method: "POST"
    })
    .then(function(success) {
      if (success) {
        commit("SET_PHONE", data.phone);
        commit("SET_PHONE_UPDATED", true);
      } else {
        commit("SET_PHONE_UPDATED", false);
      }
      commit("SET_UPDATING_PHONE", false);
    })
    .catch(function(e) {
      commit("SET_UPDATING_PHONE", false);
      commit("SET_PHONE_UPDATED", false);
    });
  } catch (e) {
    commit("SET_UPDATING_PHONE", false);
    commit("SET_PHONE_UPDATED", false);
  }
};

const setPhoneUpdated = async ({ commit }, status) => {
  commit("SET_PHONE_UPDATED", status);
};

const setCreditCardDate = async ({ commit, dispatch }) => {
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/users/update_pre_registered_credit_card_date",
      method: "POST"
    });
    return resp;
  } catch (error) {
    return error.response;
  }
};

const rejectPasswordChange = ({commit}, data) => {
  const url =  process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/users/reject_password_change"
  // console.log("URLL", url)
  Axios.put(url, {user_id: data.user, token: data.token})
}

const getWallet = async ({ commit, dispatch }) => {
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/user_bank_accounts/user_bank_wallet",
      method: "GET"
    });
    return resp.data;
  } catch (error) {
    return error.response;
  }
};

const getRequiredData = async ({ commit }) => {
  try {
    await Axios.get(process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + '/user_to_ask/?type=credit_card')
    .then((resp) => {
      commit("SET_USER_REQUIRED_DATA", resp.data);
    });
  } catch (err) {
    console.log('error', err);
  }
};

const getShippingAdress = async ({ commit }) => {
  try {
    await Axios.get(process.env.VUE_APP_API_CREDIT_CARD_URL + '/request_credit_cards').
    then((resp) => {
      if(resp.data.response.length > 0) {
        commit("SET_SHIPPING_ADDRESS", resp.data.response[0]);
        console.log('SET_SHIPPING_ADDRESS');
      }
    });
  } catch (err) {
    console.log('error', err);
  }
};

const getShippingAddressStatus = async ({ commit }) => {
  try {
    await Axios.get(process.env.VUE_APP_API_CREDIT_CARD_URL + '/request_credit_cards/status_delivery_address')
    .then((resp) => {
      commit("SET_SHIPPING_ADDRESS_STATUS", resp.data);
    });
  } catch (err) {
    console.log('error', err);
  }
};

const requestCreditCard = async ({ commit, store, dispatch }, data) => {
  try {
    await Axios.post(process.env.VUE_APP_API_CREDIT_CARD_URL + '/report_card', data)
    .then((resp) => {
      if(data.step === '2') commit("SET_SHIPPING_ADDRESS_STATUS", { result: true });
    });
  } catch (err) {
    dispatch("snackbar/showSnackbar", { message: err.response.data.errors[0].message }, { root:true });
  }
};

const setAlias = async ({ commit, state }, data) => {
  try {
    let resp = await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/bank_accounts/update_cvu_to_user",
      data: {alias: data.data.alias, cvu: data.data.cvu, currency:data.currency},
      method: "POST"
    });
    return resp.data;
  } catch (error) {
    return error.response;
  }
};

const setNewPhone = async ({commit, state, dispatch}, data) => {
  try {
    await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/phone_verification/set-new-phone",
      data: data,
      method: "POST"
    })
    .then(async function (success) {
      if (success) {
        await dispatch('getCurrent');
        await dispatch("canEditPhone");
      }
    })
    .catch(function (e) {
    })
  } catch (e) {
  }
}

const sendPhoneVerificationCode = async ({ commit, state }, data) => {
  commit("SET_INVALID_CODE", false);
  try {
    await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/phone_verification/phone-verification-step",
      data: data,
      method: "GET"
    })
    .then(function (success) {
      if (success) {

      }
    })
    .catch(function (e) {
    })
  } catch (e) {
  }
}

const verifyPhone = async ({ commit, dispatch, state }, data) => {
  commit("SET_VERIFYING_PHONE", true);
  try {
    await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/phone_verification/verify-phone-code",
      data: data,
      method: "POST"
    })
    .then(function (success) {
      if (success) {
        if (success.data.status == "success") {
          commit("SET_INVALID_CODE", false);
          dispatch("getCurrent");
          dispatch("canEditPhone");
        } else {
          commit("SET_INVALID_CODE", true);
        }
      } else {
        commit("SET_INVALID_CODE", true);
      }
      setTimeout(() => {
        commit("SET_VERIFYING_PHONE", false);
      }, 3000);
    })
    .catch(function (e) {
      commit("SET_VERIFYING_PHONE", false);
      commit("SET_INVALID_CODE", true);
    })
  } catch (e) {
    commit("SET_VERIFYING_PHONE", false);
    commit("SET_INVALID_CODE", true);
  }
}

const canEditPhone = async ({commit, dispatch, state}, data) => {
  try {
    await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/phone_verification/can-edit-phone",
      method: "GET"
    })
    .then(function (success) {
      if (success) {
        dispatch("getCurrent");
        commit("SET_CAN_EDIT_PHONE", success.data.status == "success");
      }
    })
    .catch(function (e) {
      commit("SET_CAN_EDIT_PHONE", false);
    })
  } catch (e) {
    commit("SET_CAN_EDIT_PHONE", false);
  }
}

const savePhone = async ({commit, dispatch, state}, data) => {
  try {
    await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/phone_verification/save-phone",
      method: "PUT",
      data: {phone: data}
    })
    .then(function (success) {
      if (success) {
        dispatch("getCurrent");
        //commit("SET_CAN_EDIT_PHONE", success.data.status == "success");
      }
    })
    .catch(function (e) {
      //commit("SET_CAN_EDIT_PHONE", false);
    })
  } catch (e) {
    //commit("SET_CAN_EDIT_PHONE", false);
  }
}

const sendBankAccountVerificationCode = async ({ commit, state }, data) => {
  commit("SET_INVALID_CODE", false);
  try {
    await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/user_bank_accounts/" + data.userBankAccountId + "/send_verification_code?through=" + data.through,
      method: "GET"
    })
    .then(function (success) {
      if (success) {

      }
    })
    .catch(function (e) {
    })
  } catch (e) {
  }
}

const verifyBankAccount = async ({ commit, dispatch, state }, data) => {
  try {
    await Axios({
      url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/user_bank_accounts/" + data.userBankAccountId + "/verify",
      data: { code: data.code },
      method: "POST"
    })
    .then(function (success) {
      if (success) {
        if (success.data.status == "success") {
          commit("SET_INVALID_CODE", false);
          dispatch("getCurrent");
          commit("SET_VERIFIED_BANK_ACCOUNT", true);
        } else {
          if (success.data.status == "error" && success.data.msg == "Bank account already verified") {
            commit("SET_INVALID_CODE", false);
            commit("SET_VERIFIED_BANK_ACCOUNT", true);
          } else {
            commit("SET_INVALID_CODE", true);
            commit("SET_VERIFIED_BANK_ACCOUNT", false);
          }
        }
      } else {
        commit("SET_INVALID_CODE", true);
        commit("SET_VERIFIED_BANK_ACCOUNT", false);
      }
    })
    .catch(function (e) {
      commit("SET_INVALID_CODE", true);
      commit("SET_VERIFIED_BANK_ACCOUNT", false);
    })
  } catch (e) {
    commit("SET_INVALID_CODE", true);
    commit("SET_VERIFIED_BANK_ACCOUNT", false);
  }
}

const searchByalias = ({commit, dispatch}, alias) => {
  return Axios({
    url: process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_API_VERSION_URL + "/user_bank_accounts/search_by_alias",
    data: { alias: alias },
    method: "GET"
  })
}

const getBankAccountTransfers = async ({ commit, state, dispatch }) => {
  dispatch("getFilteredBankAccountTransfers");
};

const getFilteredBankAccountTransfers = async ({ commit, state, dispatch }) => {

  let perPage = process.env.VUE_APP_ITEMS_PER_PAGE;

  if (state.currentPage == 0) {

    let page = 1;
    try {

      commit("SET_LOADER", true);

      commit("SET_TOTAL_ITEMS", 0);
      commit("SET_LAST_PAGE", 0);
      commit("SET_TRANSACTIONS", []);
      commit("SET_CURRENT_PAGE", 0);

      let scope = await Withdrawal.select([
        'amount', 'currency_to', 'currency', 'user_bank_account_id',
        'raw_response', 'real_withdrawal_date', 'titular', 'data_for_api',
        'date', 'name','issued', 'type', 'origen_cuit', 'origen_cbu',
        'to_cbu', 'bank_to', 'to_cuit', 'date_hour', 'action_id', 'real_currency', 'format_amount', 'real_date'
      ])
          .where({ 'type': 'BankWithdrawal'})
          .where({ 'issued': true })
          .page(page)
          .per(perPage)
          .order({ real_withdrawal_date: 'desc' })
          .all();

      let data = scope.data;

      let scopeCount = await Withdrawal
          .where({ 'type': 'BankWithdrawal'}).per(0).stats({
            total: "count"
          })
          .all();

      const totalItems = scopeCount.meta.stats.total.count;

      commit("SET_TOTAL_ITEMS", totalItems);
      if (totalItems > 0) {
        let lastPage = Math.ceil(totalItems / perPage);
        commit("SET_LAST_PAGE", lastPage);
      }
      commit("SET_TRANSACTIONS", data);
      commit("SET_CURRENT_PAGE", page);

      commit("SET_LOADER", false);
    } catch (err) {
      commit("SET_LOADER", false);
      return err;
    }
  } else if (state.currentPage <= state.lastPage) {
    let page = state.currentPage;

    try {
      commit("SET_LOADER", true);

      let scope = await Withdrawal.select([
        'amount', 'currency_to',  'currency', 'user_bank_account_id',
        'raw_response', 'real_withdrawal_date', 'titular', 'data_for_api',
        'date', 'name', 'issued', 'type', 'origen_cuit', 'origen_cbu',
        'to_cbu', 'bank_to', 'to_cuit', 'date_hour', 'action_id', 'real_currency', 'format_amount', 'real_date'
      ])
          .where({ 'type': 'BankWithdrawal'})
          .where({ 'issued': true })
          .page(page)
          .per(perPage)
          .includes('user_bank_account')
          .order({ real_withdrawal_date: 'desc' })
          .all();
      let data = scope.data;
      console.log(data, '5')
      commit("SET_TRANSACTIONS", data);
      commit("SET_CURRENT_PAGE", page);
      commit("SET_LOADER", false);
    } catch (err) {
      commit("SET_LOADER", false);
      return err;
    }
  }
};

const setCurrentPage = async ({ commit, dispatch }, page) => {
  commit("SET_CURRENT_PAGE", page);
  commit("CLEAN_ACTIONS");
  dispatch("getFilteredBankAccountTransfers");
};

const setSelectedItem = async ({ commit, dispatch }, item) => {
  commit("SET_SELECTED_ITEM", '');
  //dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  let extra = await Action.select(['id']).where({id: item.id}).first();
 // commit("SET_EXTRA_FIELDS", extra.data)
  commit("SET_SELECTED_ITEM", item);
  //dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
  //dispatch("setToggleDialog");
};

export default {
  login,
  forgotPassword,
  resetPassword,
  register,
  setToken,
  checkIsLogin,
  setTokenFromQuery,
  logout,
  getCurrent,
  checkIsHater,
  getCurrentLatamMilesDetail,
  getNews,
  updateSaw,
  getNewsList,
  clearNewsList,
  getUserBankAccounts,
  getVerifiedUserBankAccounts,
  cleanUserBankAccounts,
  getBanksList,
  setUserNewBankAccount,
  deleteUserBankAccount,
  setUserData,
  getDocFields,
  saveDocFiels,
  saveAvatar,
  setShowHideGetPhoneDialog,
  setUserStatistics,
  updatePhone,
  setPhoneUpdated,
  setCreditCardDate,
  rejectPasswordChange,
  getWallet,
  setAlias,
  setNewPhone,
  sendPhoneVerificationCode,
  verifyPhone,
  canEditPhone,
  savePhone,
  sendBankAccountVerificationCode,
  verifyBankAccount,
  searchByalias,
  getBankAccountTransfers,
  getFilteredBankAccountTransfers,
  setCurrentPage,
  setSelectedItem,
  updateVisibleUserBankAccount,
  getRequiredData,
  getShippingAdress,
  getShippingAddressStatus,
  requestCreditCard
};
