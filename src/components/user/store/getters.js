const isLoggedIn = state => state.isLoggedIn;
const current = state => state.current;
const news = state => state.news;
const newsPage = state => state.newsPage;
const currentPage = state => state.currentPage;

const isOldUser = state => {
  let newUsersDate = new Date(process.env.VUE_APP_NEW_USERS_DATE);
  let userCreatedAt = state.isLoggedIn ?
    new Date(state.current.created_at) :
    new Date()
  ;
  return newUsersDate > userCreatedAt ? true : false;
}

export default {
  isLoggedIn,
  current,
  news,
  newsPage,
  currentPage,
  isOldUser
};
