const UPDATE_BREADCRUMBS = (state, breadcrumbs) => {
  state.uiBreadcrumbs = breadcrumbs;
};

const REMOVE_BREADCRUMBS = state => {
  state.uiBreadcrumbs = true;
};

export default {
  UPDATE_BREADCRUMBS,
  REMOVE_BREADCRUMBS
};
