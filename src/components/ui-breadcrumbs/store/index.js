import actions from "./actions";
import mutations from "./mutations";

const state = {
  uiBreadcrumbs: []
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};