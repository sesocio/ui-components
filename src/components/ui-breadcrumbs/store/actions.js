const setBreadcrumbs = ({ commit }, breadcrumbs) => {
  commit("UPDATE_BREADCRUMBS", breadcrumbs);
};

const cleanBreadcrumbs = ({ commit }) => {
  commit("REMOVE_BREADCRUMBS");
};

export default {
  setBreadcrumbs,
  cleanBreadcrumbs
};
