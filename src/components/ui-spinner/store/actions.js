const showSpinnerLoader = ({ commit }, payload) => {
  commit("SHOW_SPINNER_LOADER");
};

const hideSpinnerLoader = ({ commit }) => {
  commit("HIDE_SPINNER_LOADER");
};

const toggleSpinnerLoader = ({ commit }) => {
  commit("TOGGLE_SPINNER_LOADER");
};

export default {
  showSpinnerLoader,
  hideSpinnerLoader,
  toggleSpinnerLoader
};
