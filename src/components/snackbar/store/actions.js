const showSnackbar = ({ commit }, payload) => {
  commit("HIDE_SNACKBAR");

  setTimeout(() => {
    commit("SHOW_SNACKBAR", payload);
  }, 120);
};

const hideSnackbar = ({ commit }) => {
  commit("HIDE_SNACKBAR");
};

export default {
  showSnackbar,
  hideSnackbar
};
