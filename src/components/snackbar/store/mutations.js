const SHOW_SNACKBAR = (state, payload) => {
  state.snackbar = payload;
  state.snackbar.visible = true;
};

const HIDE_SNACKBAR = state => {
  state.snackbar = {...state.snackbar};
  state.snackbar.visible = false;
};

export default {
  SHOW_SNACKBAR,
  HIDE_SNACKBAR
};
