import actions from "./actions";
import mutations from "./mutations";

const state = {
  snackbar: {}
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};