import actions from "./actions";
import mutations from "./mutations";

const state = {
  drawer: false,
  isPwa: false,
  pwaUrl: null,
  cardStatus: null,
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
