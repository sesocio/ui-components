import axios from "axios";

const showDrawer = ({ commit }) => {
  commit("SHOW_DRAWER");
};

const hideDrawer = ({ commit }) => {
  commit("HIDE_DRAWER");
};

const setIsPwa = ({ commit }) => {
  commit("SET_IS_PWA");
};

const setPwaUrl = ({ commit }) => {
  commit("SET_PWA_URL");
};

const getCardStatus = ({ commit }) => {
  let url = `${process.env.VUE_APP_API_CREDIT_CARD_URL}/get_card_status_menu_actionable`;

  axios.get(url).then((resp) => {
    commit("SET_CARD_STATUS", resp.data.action);
  }).catch(err => {
    console.log("error", err);
  });
};

export default {
  showDrawer,
  hideDrawer,
  setIsPwa,
  setPwaUrl,
  getCardStatus
};