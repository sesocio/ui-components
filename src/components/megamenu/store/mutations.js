const SHOW_DRAWER = state => {
  state.drawer = !state.drawer;
};

const HIDE_DRAWER = state => {
  state.drawer = false;
};

const SET_IS_PWA = (state, payload) => {
  state.isPwa = payload;
};

const SET_PWA_URL = (state, payload) => {
  state.pwaUrl = payload;
};

const SET_CARD_STATUS = (state, payload) => {
  state.cardStatus = payload;
};

export default {
  SHOW_DRAWER,
  HIDE_DRAWER,
  SET_IS_PWA,
  SET_PWA_URL,
  SET_CARD_STATUS,
};
