const SET_CONTENT_HEADER = (state, payload) => {
  state.contentHeader = payload;
};

export default {
  SET_CONTENT_HEADER
};