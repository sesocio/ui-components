const setContentHeader = ({ commit }, payload) => {
  commit("SET_CONTENT_HEADER", payload);
};

export default {
  setContentHeader
};