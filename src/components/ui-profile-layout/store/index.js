import actions from "./actions";
import mutations from "./mutations";

const state = {
  /* Especifica el título del componente a mostrar.
   * desktop: booleano que especifica si el título se muestra o no en versión desktop
   * title y subtitle: nombre del título y/o subtitulo a mostrar
   * currencies: booleano que especifica si el listado de monedas se muestra o no (solo en mobile)
   */
  contentHeader: {
    desktop: false,
    title: null,
    subtitle: null,
    currencies: false,
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};