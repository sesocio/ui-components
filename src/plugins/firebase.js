import firebase from 'firebase/app';

const firebaseConfig = {
  apiKey: "AIzaSyBaTLGAEGUa_jLUYE3GuNqRnZQFrXlWsWg",
  authDomain: "sesocio-483bb.firebaseapp.com",
  databaseURL: "https://sesocio-483bb.firebaseio.com",
  projectId: "sesocio-483bb",
  storageBucket: "sesocio-483bb.appspot.com",
  messagingSenderId: "971171706195",
  appId: "1:971171706195:web:c50bf0a0872f81658e1da6"
};

// Initialize Firebase
if (firebase.apps.length === 0) {
  firebase.initializeApp(firebaseConfig);
}