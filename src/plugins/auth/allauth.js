import Vue from "vue";
import VueAllAuth from "./vue-all-auth";
Vue.use(VueAllAuth, {
    google: {
        // keys for google
        client_id: "565094347045-fbc9scgbqk1qg3l579fa6069nhqpk5ia.apps.googleusercontent.com", //VARIABLE DE ENTORNO
        scope: "profile email",
    },
    facebook: {
        // keys for fb
        appId: process.env.VUE_APP_FACEBOOK_APP_ID,
        cookie: true,
        xfbml: true,
        version: "v3.2",
        locale: "es_ES",
    },
    twitter: {
        // keys for twitter
    },
    github: {
        // keys for github
    }

});
// Vue.allAuth().google().init();
// Vue.allAuth().facebook().init();
