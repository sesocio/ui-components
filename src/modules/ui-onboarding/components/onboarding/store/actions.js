import Axios from 'axios';

const openOnboardingDialog = async ({ commit }, redirectUrl) => {
  commit('OPEN_ONBOARDING_DIALOG', {'redirectUrl': redirectUrl})
};

const openOnboardingLogin = async ({ commit }) => {
  commit('OPEN_ONBOARDING_LOGIN')
};

const openOnboardingRegister = async ({ commit }) => {
  commit('OPEN_ONBOARDING_REGISTER')
};

const closeOnboardingDialog = async ({ commit, rootState }) => {
  commit('CLOSE_ONBOARDING_DIALOG', {'rootData': rootState.onboarding})
};

const goToDialogStep = async ({ commit }, value) => {
  commit('GO_TO_DIALOG_STEP', value)
};

const goToInitStep = async ({ commit }, value) => {
  commit('GO_TO_INIT_STEP', value)
};

const nextStepDialog = async ({ commit }) => {
  commit('NEXT_STEP')
};

const backStepDialog = async ({ commit }) => {
  commit('BACK_STEP')
};

const getCardStatus = async ({ commit }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_CREDIT_CARD_URL}/get_card_status`;

      const response = await Axios.get(url)
      commit('SET_CARD_STATUS', response.data)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

export default {
  openOnboardingDialog,
  openOnboardingLogin,
  openOnboardingRegister,
  closeOnboardingDialog,
  goToDialogStep,
  goToInitStep,
  nextStepDialog,
  backStepDialog,
  getCardStatus
};
