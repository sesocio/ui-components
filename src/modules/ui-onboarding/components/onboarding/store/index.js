import actions from "./actions";
import mutations from "./mutations";

const state = {
  'open': false,
  'current_step_dialog': 1,
  'initStep': 1,
  'debugMode': false,
  'cardStatus': null,
  'redirectPath': null
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};