const OPEN_ONBOARDING_DIALOG = (state, data) => {
  state.open = true;
  state.current_step_dialog = 1;
  if (data.redirectUrl) {
    state.redirectUrl = data.redirectUrl;
  }
};

const OPEN_ONBOARDING_LOGIN = (state) => {
  state.open = true;
  state.current_step_dialog = 1;
};

const OPEN_ONBOARDING_REGISTER = (state) => {
  state.open = true;
  state.current_step_dialog = 2;
};

const CLOSE_ONBOARDING_DIALOG = (state, data) => {
  state.open = false;
  state.current_step_dialog = 1;
  state.initStep = 1;
  data.rootData.current_step = 1;
  state.redirectPath = null;
};

const GO_TO_DIALOG_STEP = (state, value) => {
  state.current_step_dialog = value;
};

const GO_TO_INIT_STEP = (state, value) => {
  state.initStep = value;
};

const NEXT_STEP = (state) => {
  state.current_step_dialog++;
};

const BACK_STEP = (state) => {
  state.current_step_dialog--;
};

const SET_CARD_STATUS = (state, data) => {
  state.cardStatus = data.action;
};

export default {
  OPEN_ONBOARDING_DIALOG,
  OPEN_ONBOARDING_LOGIN,
  OPEN_ONBOARDING_REGISTER,
  CLOSE_ONBOARDING_DIALOG,
  GO_TO_DIALOG_STEP,
  GO_TO_INIT_STEP,
  NEXT_STEP,
  BACK_STEP,
  SET_CARD_STATUS
};
