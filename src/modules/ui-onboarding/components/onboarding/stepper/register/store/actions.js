import Axios from 'axios';
import { User } from "../../../../../../../components/core/models/user.model";

const setMaxStep = async ({ commit }, value) => {
  commit('SET_MAX_STEP', value)
};

const goToStep = async ({ commit }, value) => {
  commit('GO_TO_STEP', value)
};

const nextStep = async ({ commit }) => {
  commit('NEXT_STEP')
};

const backStep = async ({ commit }) => {
  commit('BACK_STEP')
};

const userRegister = async ({ commit, rootState, dispatch }, user) => {
  return new Promise(async(resolve, reject)=>{
    try {
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/users`;
 
      const response = await Axios.post(url, {
        'user': {
          'firstname': user.firstname,
          'lastname': user.lastname,
          'email': user.email.toLowerCase().trim(),
          'password': user.password,
          'password_confirmation': user.password_confirmation,
          'country': rootState.core.country,
          'referral_code': user.referral_code,
          'terms': user.terms,
          'new_onboarding': user.new_onboarding,
          "user_statistic_attributes": {
            "source": "",
            "medium": "",
            "campaign": "",
            "term": "",
            "content": ""
          }
        }
      })

      // login
      const token = response.data.token;
      await dispatch("user/setToken", token, { root: true });

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const checkCodeReferral = async ({ commit }, user) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/users/validate_referral_code/${user.referral_code}`;

      const response = await Axios.get(url)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const sendValidateEmail = async ({ commit, dispatch }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/users/send_confirmation_instructions_email`;

      const response = await Axios.get(url)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const verifyEmailCode = async ({ commit, rootState, dispatch }, code) => {
  return new Promise(async(resolve, reject)=>{
    try {
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/users/validate_email_code`;

      const response = await Axios.post(url, {
        'code': code
      })

      commit('ADD_CONFIRMED_AT', {'confirmed_at': response.data.confirmed_at, 'rootData': rootState.user.current})

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const checkConfirmedAt = async ({ commit, rootState }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/users?page[size]=1&fields[users]=confirmed_at`;

      const response = await Axios.get(url)

      let confirmed_at = response.data.data[0].attributes.confirmed_at;
      commit('ADD_CONFIRMED_AT', {'confirmed_at': confirmed_at, 'rootData': rootState.user.current})

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const updatePhone = async ({ commit, rootState, dispatch }, phone) => {
  return new Promise(async(resolve, reject)=>{
    try {
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/users/phone`;

      const response = await Axios.post(url, {
        'phone': phone
      })

      commit('SET_PHONE_ONBOARDING', {'phone': phone, 'rootData': rootState.user.current})

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const sendPhoneCode = async ({ commit, dispatch }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/phone_verification/phone-verification-step`;

      const response = await Axios.get(url)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const verifyPhoneCode = async ({ commit, rootState, dispatch }, code) => {
  return new Promise(async(resolve, reject)=>{
    try {
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/phone_verification/verify-phone-code`;

      const response = await Axios.post(url, {
        'code': code
      })

      commit('SET_PHONE_STATUS_ONBOARDING', {'rootData': rootState.user.current})

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getCuilByDniAndSex = async ({ commit }, data) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/users/get_cuil`;

      const response = await Axios.get(url, {
        params: {
          'dni': data.dni,
          'sex': data.sex[0],
        }
      })

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const sendGeneralDetails = async ({ commit, dispatch, rootState }, newUser) => {
  return new Promise(async(resolve, reject)=>{
    try {
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
      let auxData = {
        'user': newUser,
        'rootData': rootState.user.current
      }
      commit('SET_GENERAL_DETAILS', auxData)

      let user = new User({id: rootState.user.userId});
      user.isPersisted = true;
      if(newUser.country.toLowerCase() == 'ar'){
        user.cuit = newUser.cuit;
      }
      if(newUser.country.toLowerCase() == 'co'){
        user.rut = newUser.rut;
        user.state = newUser.state;
        user.city = newUser.city;
        user.address = newUser.address;
        user.declarant = (newUser.declarant == '') ? false : newUser.declarant;
      }
      user.document = newUser.document;
      user.gender = newUser.gender;
      user.country = newUser.country;
      user.occupation = newUser.occupation;
      user.civilStatus = newUser.civilStatus;
      user.facta = newUser.facta;
      user.politicallyExposed = newUser.politicallyExposed;
      user.obligatedSubject = newUser.obligatedSubject;
      user.legalEntity = newUser.legalEntity;

      // birthdate
      const [year, month, day] = newUser.birthdate.split('-');
      user.birthdate = `${day}/${month}/${year}`;

      const response = await user.save()
      await dispatch("user/getCurrent", {}, {root:true});
      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getStatesByCountry = async ({ commit }, country) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/users/get_states_by_country/${country.toLowerCase()}`;

      const response = await Axios.get(url)

      commit('SET_STATES_BY_COUNTRY', response.data)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getCitiesByState = async ({ commit }, data) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/users/get_cities_by_state/${data.country.toLowerCase()}/${data.state.toLowerCase()}`;

      const response = await Axios.get(url)

      commit('SET_CITIES_BY_STATE', response.data)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const saveUserAddress = async ({ commit, dispatch, rootState }, data) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let user = new User({id: rootState.user.userId});
      user.isPersisted = true;
      if (data.provider == 'google') {
        user.googleMapsJson = {
          address: data.address
        };
        user.postalCode = data.postal_code;
        user.streetFloor = data.floor;
        user.streetFloorApartment = data.apartment;
      }else {
        user.address = data.address;
        user.street = data.street;
        user.streetNumber = data.street_number;
        user.postalCode = data.postal_code;
        user.streetFloor = data.floor;
        user.streetFloorApartment = data.apartment;
        user.city = data.city;
        user.state = data.state;
      }

      const response = await user.save()
      await dispatch("user/getCurrent", {}, {root:true});
      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const sendDocuments = async ({ commit, dispatch }, image) => {
  return new Promise(async(resolve, reject)=>{
    try {
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/documents`;

      let response = await Axios.post(url, image)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getDocumentStatus = async ({ commit, dispatch }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/documents`;

      let response = await Axios.get(url)

      commit('SET_USER_DOCUMENTS', response.data)
      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const sendDeclaration = async ({ commit, dispatch, rootState }, declaration) => {
  return new Promise(async(resolve, reject)=>{
    try {
      dispatch("core/toggleShowHideLoaderSpinner", null, { root: true });
      let auxData = {
        'declaration': declaration,
        'rootData': rootState.user.current
      }
      commit('SET_DECLARATION', auxData)

      let user = new User({id: rootState.user.userId});
      user.politicallyExposed = rootState.user.current.obligated_subject;
      user.relationPoliticallyExposed = rootState.user.current.relation_politically_exposed;
      user.fatca = rootState.user.current.fatca;

      const response = await user.save()
      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const requestCreditCard = async ({ commit, rootState }, data) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_CREDIT_CARD_URL}/request_credit_cards`;

      let response = await Axios.post(url, data)

      let auxData = {
        'result': true,
        'rootData': rootState.user.current
      }
      commit('SET_SHIPPING_ADDRESS_STATUS_ONBOARDING', auxData)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const requestCreditCardSameAddress = async ({ commit, rootState }) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_CREDIT_CARD_URL}/request_credit_cards`;

      let response = await Axios.post(url, {
        "same_address": true
      })

      let auxData = {
        'result': true,
        'rootData': rootState.user.current
      }
      commit('SET_SHIPPING_ADDRESS_STATUS_ONBOARDING', auxData)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getGenderByName = async ({ commit }, name) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/dictionary_name?name=${name}`;

      let response = await Axios.get(url)

      commit('SET_GENDER_BY_NAME', response.data)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const getBankAccountInfo = async ({ commit, rootState }, name) => {
  return new Promise(async(resolve, reject)=>{
    try {
      let url = `${process.env.VUE_APP_API_BASE_URL}${process.env.VUE_APP_API_VERSION_URL}/bank_accounts/bank_account_for_user`;

      let response = await Axios.get(url, {
        params: {
          currency: rootState.user.current.local_currency,
          amount: null,
          account_type: "cbu",
          project_id: null
        }
      })

      commit('SET_BANK_ACCOUNT_INFO', response.data)

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

export default {
  setMaxStep,
  goToStep,
  nextStep,
  backStep,
  userRegister,
  verifyEmailCode,
  checkConfirmedAt,
  checkCodeReferral,
  sendValidateEmail,
  updatePhone,
  sendPhoneCode,
  verifyPhoneCode,
  getCuilByDniAndSex,
  sendGeneralDetails,
  saveUserAddress,
  getStatesByCountry,
  getCitiesByState,
  sendDocuments,
  getDocumentStatus,
  sendDeclaration,
  requestCreditCard,
  requestCreditCardSameAddress,
  getGenderByName,
  getBankAccountInfo
};
