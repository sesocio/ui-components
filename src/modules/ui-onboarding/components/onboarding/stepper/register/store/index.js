import actions from "./actions";
import getters from "./getters";
import mutations from "./mutations";

const state = {
  'current_step': 1,
  'max_step': 1,
  'states_by_country': [],
  'cities_by_state': [],
  'gender_by_name': '',
  'bankAccount': null,
  'userDocuments': null,
  'hasDocuments': false
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations
};