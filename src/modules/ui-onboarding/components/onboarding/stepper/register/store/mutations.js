const INIT_STEP = (state) => {
  state.current_step = 1;
};

const GO_TO_STEP = (state, value) => {
  state.current_step = value;
};

const SET_MAX_STEP = (state, value) => {
  state.max_step = value;
};

const NEXT_STEP = (state) => {
  if (state.current_step < state.max_step) {
    state.current_step++;
  }
};

const BACK_STEP = (state) => {
  if (state.current_step > 1) {
    state.current_step--;
  }
};

const ADD_CONFIRMED_AT = (state, data) => {
  data.rootData.confirmed_at = data.confirmed_at
};

const SET_PHONE_ONBOARDING = (state, data) => {
  data.rootData.phone = data.phone
}

const SET_PHONE_STATUS_ONBOARDING = (state, data) => {
  data.rootData.phone_verification_step = 'phone_verified'
}

const SET_GENERAL_DETAILS = (state, data) => {
  data.rootData.document = data.user.document
  data.rootData.cuit = data.user.cuit
  data.rootData.rut = data.user.rut
  data.rootData.gender = data.user.gender
  data.rootData.country = data.user.country
  data.rootData.state = data.user.state
  data.rootData.city = data.user.city
  data.rootData.address = data.user.address
  data.rootData.declarant = data.user.declarant
  const [year, month, day] = data.user.birthdate.split('-');
  data.rootData.birthdate = `${day}/${month}/${year}`
}

const SET_STATES_BY_COUNTRY = (state, value) => {
  state.states_by_country = value;
};

const SET_CITIES_BY_STATE = (state, value) => {
  state.cities_by_state = value;
};

const SET_DECLARATION = (state, data) => {
  data.rootData.obligated_subject = data.declaration.obligatedSubject
  data.rootData.relation_politically_exposed = data.declaration.politicallyExposed
  data.rootData.fatca = data.declaration.facta
}

const SET_SHIPPING_ADDRESS_STATUS_ONBOARDING = (state, data) => {
  data.rootData.shippingAddressStatus = data.result
}

const SET_GENDER_BY_NAME = (state, payload) => {
  if (payload.response.gender == 'F') {
    state.gender_by_name = 'female';
  } else if (payload.response.gender == 'M') {
    state.gender_by_name = 'male';
  } else {
    state.gender_by_name = '';
  }
};

const SET_BANK_ACCOUNT_INFO = (state, value) => {
  state.bankAccount = value;
};

const SET_USER_DOCUMENTS = (state, value) => {
  state.userDocuments = value;
  if (value.fields.id_back.file != '' && value.fields.id_front.file != '' && value.fields.selfie.file != '') {
    state.hasDocuments = true;
  }else {
    state.hasDocuments = false;
  }
};

export default {
  INIT_STEP,
  GO_TO_STEP,
  SET_MAX_STEP,
  NEXT_STEP,
  BACK_STEP,
  ADD_CONFIRMED_AT,
  SET_PHONE_ONBOARDING,
  SET_PHONE_STATUS_ONBOARDING,
  SET_GENERAL_DETAILS,
  SET_STATES_BY_COUNTRY,
  SET_CITIES_BY_STATE,
  SET_DECLARATION,
  SET_SHIPPING_ADDRESS_STATUS_ONBOARDING,
  SET_GENDER_BY_NAME,
  SET_BANK_ACCOUNT_INFO,
  SET_USER_DOCUMENTS
};
