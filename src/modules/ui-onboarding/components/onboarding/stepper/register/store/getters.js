const isAdult = (state, getters, rootState) => {
  let minDate = new Date(new Date().setFullYear(new Date().getFullYear() - 18));
  let currentDate = new Date(rootState.user.current.birthdate)
  if (minDate - currentDate >= 18) {
    return true
  }else {
    return false
  }
}

const checkData = (state, getters, rootState) => (data) => {
  let auxReturn = false;
  for (let i = 0; i < data.length; i++) {
    const column = data[i]
    const key = Object.keys(column)[0]
    if (key == 'phone_verification_step') {
      if (rootState.user.current[key] != column[key]) {
        auxReturn = true;
        break
      }
    }else if (key == 'birthdate' && column[key] != null) {
      if (!getters.isAdult && !rootState.onboarding.isAuthorizing && rootState.user.current.country == 'AR') {
        auxReturn = true;
        break
      }
    }else if (key == 'sesocio_card' && column[key] != null) {
      if (['solicitar', null].includes(rootState.onboarding_dialog.cardStatus) && rootState.user.current.country == 'AR') {
        auxReturn = true;
        break
      }
    }else {
      if (rootState.user.current[key] == column[key]) {
        auxReturn = true;
        break
      }
    }
  }
  return auxReturn;
}

const checkUserDataStep = (state, getters, rootState) => (isOnboarding = false) => {
  const rules = {
    'step2': [{
      confirmed_at: null
    }],
    'step3': [{
      phone: null
    }],
    'step4': [{
      phone_verification_step: 'phone_verified'
    }],
    'step5-ar': [{
      document: null
    }, {
      gender: null
    }, {
      birthdate: null
    }, {
      country: null
    }, {
      cuit: null
    }],
    'step5-co': [{
      document: null
    }, {
      gender: null
    }, {
      birthdate: null
    }, {
      country: null
    }, {
      rut: null
    }, {
      state: null
    }, {
      city: null
    }, {
      address: ''
    }, {
      declarant: null
    }],
    'step5-default': [{
      document: null
    }, {
      gender: null
    }, {
      birthdate: null
    }, {
      country: null
    }],
    'step6-ar': [{
      address: ''
    }],
    'step8': [{
      all_documents_approved: false
    }],
    'step9': [{
      birthdate: true
    }],
    'step10': [{
      sesocio_card: true
    }],
    'default': []
  }
  // Default StepContentCardDelivery
  let auxReturn = null;

  if(getters.checkData(rules['step2'] || rules['default'])) {
  // StepContentValidateEmail
    auxReturn = 2;
  }else if(getters.checkData(rules['step3'] || rules['default'])) {
  // StepContentActivateCellPhone
    auxReturn = 3;
  }else if(getters.checkData(rules['step4'] || rules['default'])) {
  // StepContentValidateCellPhone
    auxReturn = 4;
  }else if(getters.checkData(rules['step5-ar'] || rules['default']) &&
    rootState.user.current.country == "AR" && (
      (
        !rootState.user.current.all_documents_approved &&
        !rootState.user.current.profile_data_completed
      ) || isOnboarding
    )
  ) {
  // StepContentGeneralDetails - AR
    auxReturn = 5;
  }else if(getters.checkData(rules['step5-co'] || rules['default']) &&
    rootState.user.current.country == "CO" && (
      (
        !rootState.user.current.all_documents_approved &&
        !rootState.user.current.profile_data_completed
      ) || isOnboarding
    )
  ) {
  // StepContentGeneralDetails - CO
    auxReturn = 5;
  }else if(getters.checkData(rules['step5-default'] || rules['default']) && (
      (
        !rootState.user.current.all_documents_approved &&
        !rootState.user.current.profile_data_completed
      ) || isOnboarding
    )
  ) {
  // StepContentGeneralDetails - DEFAULT
    auxReturn = 5;
  }else if(getters.checkData(rules['step6-ar'] || rules['default']) &&
    rootState.user.current.country == "AR" && (
      (
        !rootState.user.current.all_documents_approved
      ) || isOnboarding
    )
  ) {
  // StepContentUserAddress - AR
    auxReturn = 6;
  }else if(getters.checkData(rules['step8'] || rules['default']) &&
    (
      (
        !rootState.user.current.all_documents_approved
      ) || isOnboarding
    )
  ) {
  // StepContentValidateIdentity
    auxReturn = 8;
  }else if(getters.checkData(rules['step10'] || rules['default']) && !rootState.user.current.has_prepaid_card) {
  // StepContentCardDelivery
    auxReturn = 10;
  }

  return auxReturn;
}

export default {
  isAdult,
  checkData,
  checkUserDataStep
};
