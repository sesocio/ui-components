const SET_GOOGLE_ADDRESS = (state, data) => {
  state.googleAddress = data;
};

const SET_PREDICTIONS = (state, data) => {
  state.googlePredictions = data
};

export default {
  SET_GOOGLE_ADDRESS,
  SET_PREDICTIONS
};
