import Axios from 'axios';

const getAddress = async ({ state, commit }, address) => {
  return new Promise(async(resolve, reject)=>{
    try {
      const response = await Axios.get(`https://maps.googleapis.com/maps/api/geocode/json`, {
        params: {
          'address': address,
          'key': process.env.VUE_APP_GOOGLE_MAPS_API_KEY
        }
      })
      commit('SET_GOOGLE_ADDRESS', response.data.results[0]);

      resolve(response)

    } catch (e) {
      reject(e)
    }
  })
};

const setPredictions = async ({ commit }, predictions) => {
  commit('SET_PREDICTIONS', predictions);
};

export default {
  getAddress,
  setPredictions
};
