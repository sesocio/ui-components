import actions from "./actions";
import mutations from "./mutations";

const state = {
  'googleAddress': null,
  'googlePredictions': []
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};