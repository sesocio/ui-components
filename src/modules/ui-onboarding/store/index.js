import actions from "./actions";
import mutations from "./mutations";

const state = {
  // uiSpinner: false
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};