export default [
  {
    path: "/login",
    name: "login",
    component: () => import(/* webpackChunkName: "UiLogin" */ '../pages/UiLogin'),
    title: "Login"
  },
  {
    path: "/register",
    name: "register",
    component: () => import(/* webpackChunkName: "Register" */ '../pages/UiRegister'),
    title: "Registrarse"
  },
  // {
  //   path: "/recover",
  //   name: "recover",
  //   component: () => import(/* webpackChunkName: "Recover" */ '../components/ForgotPassword'),
  //   title: "Recover"
  // },
  // {
  //   path: "/reset/password/:token",
  //   name: "reset",
  //   component: () => import(/* webpackChunkName: "Reset" */ '../components/ResetPassword'),
  //   title: "Reset"
  // }
];
