// UI Components
import UiFooter from './src/components/footer';
import Snackbar from './src/components/snackbar/Snackbar';
import Wrapper from './src/components/core/Wrapper';
import CurrencyButton from './src/components/core/CurrencyButton';
import NotAvailable from './src/components/NotAvailable';
import VueTelInputVuetify from './src/plugins/vue-tel-input-vuetify/lib/vue-tel-input-vuetify';
import VueTelInput from './src/plugins/vue-tel-input-vuetify/lib/vue-tel-input';
import UserStore from './src/components/user/store/index.js';
import UiMegamenu from './src/components/megamenu';
import Security from './src/components/security/Security';
import UiSpinner from './src/components/ui-spinner';
import UiIntercom from './src/components/ui-intercom';
import UiRecommendedActions from './src/components/recommendedActions';
import UiBreadcrumbs from './src/components/ui-breadcrumbs';
import UiPagination from './src/components/ui-pagination';

// Layouts
import UiProfileLayout from './src/components/ui-profile-layout';
import LayoutContent from './src/components/ui-profile-layout/components/LayoutContent';

// SprayPaint Models
import ApplicationRecord from './src/components/core/models/application_record';
import Action from './src/components/core/models/action.model';
import Actionable from './src/components/core/models/actionable.model';
import ArticleComment from './src/components/core/models/article_comment.model';
import ArticleProject from './src/components/core/models/article_project.model';
import Article from './src/components/core/models/article.model';
import Banks from './src/components/core/models/banks.model';
import Banner from './src/components/core/models/banner.model';
import Category from './src/components/core/models/category.model';
import Checkout from './src/components/core/models/checkout.model';
import Deposit from './src/components/core/models/deposit.model';
import Investment from './src/components/core/models/investment.model';
import NftAuthor from './src/components/core/models/nft_author.model';
import NftCollection from './src/components/core/models/nft_collection.model';
import Portfolio from './src/components/core/models/portfolio.model';
import Project from './src/components/core/models/project.model';
import Ranking from './src/components/core/models/ranking.model';
import TradingBuyOriginal from './src/components/core/models/trading_buy_original.model';
import TradingBuy from './src/components/core/models/trading_buy.model';
import TradingSaleOriginal from './src/components/core/models/trading_sale_original.model';
import TradingSale from './src/components/core/models/trading_sale.model';
import Trading from './src/components/core/models/trading.model';
import UserBankAccounts from './src/components/core/models/user_bank_account.model';
import User from './src/components/core/models/user.model';
import UserNews from './src/components/core/models/user.news.model';
import VestingAssignments from './src/components/core/models/vesting_assignments.model';
import Withdrawal from './src/components/core/models/withdrawal.model';

// UI Onboarding
import OnboardingModal from './src/modules/ui-onboarding/components/onboarding/modal/OnboardingModal';
import OnboardingLogin from './src/modules/ui-onboarding/components/onboarding/stepper/login/OnboardingLogin';
import HeaderItem from './src/modules/ui-onboarding/components/onboarding/stepper/register/misc/HeaderItem';
import GoogleAutocomplete from './src/modules/ui-onboarding/components/onboarding/stepper/register/misc/address-components/google/GoogleAutocomplete';
import PlacesAutocomplete from './src/modules/ui-onboarding/components/onboarding/stepper/register/misc/address-components/gob-ar/PlacesAutocomplete';
// Login, Register, Recover
import UiOnboardingModule from './src/modules/ui-onboarding/routes/';

export {
	// UI Components
	UiFooter,
	Snackbar,
	Wrapper,
	CurrencyButton,
	NotAvailable,
	OnboardingModal,
	OnboardingLogin,
	HeaderItem,
	GoogleAutocomplete,
	PlacesAutocomplete,
	VueTelInputVuetify,
	VueTelInput,
	UserStore,
	UiMegamenu,
	Security,
	UiSpinner,
	UiIntercom,
	UiRecommendedActions,
	UiBreadcrumbs,
	UiPagination,
	// SprayPaint Models
	ApplicationRecord,
	Action,
	Actionable,
	ArticleComment,
	ArticleProject,
	Article,
	Banks,
	Banner,
	Category,
	Checkout,
	Deposit,
	Investment,
	NftAuthor,
	NftCollection,
	Portfolio,
	Project,
	Ranking,
	TradingBuyOriginal,
	TradingBuy,
	TradingSaleOriginal,
	TradingSale,
	Trading,
	UserBankAccounts,
	User,
	UserNews,
	VestingAssignments,
	Withdrawal,
	// Pages
	UiOnboardingModule, // Login, Register, Recover,
	// Layouts
	UiProfileLayout,
	LayoutContent
};