# sesocio-ui-components

## Setup
Add to package.json
```
"ui-components": "git+https://bitbucket.org/sesocio/ui-components.git#semver:^1.0.0",
```

### Hot-reloads for development
Replace the "ui-components" line from your package.json with this:
```
"ui-components": "file:../ui-components",
```

Or alternatively this package can be linked from a local folder without edit the package.json, see a example below:

```
cd ~HOME/development/sesocio/ui-components
yarn link
cd ~HOME/development/sesocio/sesocio-pwa/node_modules
yarn link "ui-components"
```

Same example in a single line:

```
cd ~HOME/development/sesocio/ui-components && yarn link && cd ../sesocio-pwa/node_modules && yarn link "ui-components"
```

### HTTPS cert localhost:
Install **mkcert** tool:
https://github.com/FiloSottile/mkcert

Run the following lines:
```
mkcert -install
mkcert localhost 127.0.0.1 ::1
```

Make a folder inside your project with the name **certs**, next move both files **localhost+2-key.pem** and **localhost+2.pem** to your new folder.
```
mv -t certs localhost+2.pem localhost+2-key.pem
```

Include in your vue.config.js the below code:
```
const fs = require('fs')

module.exports = {
    devServer: {
        https: {
          key: fs.readFileSync('./certs/localhost+2-key.pem'),
          cert: fs.readFileSync('./certs/localhost+2.pem'),
        },
        public: 'https://localhost:8080/'
    }
}
```

Run your app with **yarn serve**

### Import components example:
```
<template>
	<ui-megamenu></ui-megamenu>
    <snackbar></snackbar>
</template>

<script>
	import { UiMegamenu, Snackbar } from 'ui-components';

	export default {
		components: {
			UiMegamenu,
            Snackbar
		},
	}
</script>
```

### Releases:
To release a new version of this package we use tags, you can list all our published versions using **git tag** command.
```
# add new tag
git tag -a 1.0.0 -m "release v1.0.0"
git push origin 1.0.0

# remove tag
git tag -d 1.0.0
git push origin -d 1.0.0
```

Or alternatively you can use our automated script **tagnow**
```
# Run the chmod command only the first time.
cd ~HOME/development/sesocio/ui-components && chmod +x tagnow

# Use the tagnow script for make and push an auto increment tag.
./tagnow
```

### Spraypaint models
- [action.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/action.model.js)
- [actionable.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/actionable.model.js)
- [application_record.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/application_record.js)
- [article_comment.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/article_comment.model.js)
- [article_project.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/article_project.model.js)
- [article.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/article.model.js)
- [banks.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/banks.model.js)
- [category.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/category.model.js)
- [checkout.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/checkout.model.js)
- [deposit.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/deposit.model.js)
- [investment.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/investment.model.js)
- [portfolio.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/portfolio.model.js)
- [project.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/project.model.js)
- [ranking.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/ranking.model.js)
- [trading_buy_original.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/trading_buy_original.model.js)
- [trading_buy.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/trading_buy.model.js)
- [trading_sale_original.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/trading_sale_original.model.js)
- [trading_sale.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/trading_sale.model.js)
- [trading.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/trading.model.js)
- [user_bank_account.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/user_bank_account.model.js)
- [user.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/user.model.js)
- [user.news.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/user.news.model.js)
- [vesting_assignments.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/vesting_assignments.model.js)
- [withdrawal.model.js](https://bitbucket.org/sesocio/ui-components/src/master/src/components/core/models/withdrawal.model.js)


### More details
See [all available components and spraypaint models](https://bitbucket.org/sesocio/ui-components/src/master/index.js).
